/**
 * Created by Семен on 28.05.14.
 */
function CompareResult(added, deleted, changed, equals) {
    this.added = added;
    this.deleted = deleted;
    this.changed = changed;
    this.equals = equals;
}

function compareForm(params1, params2) {
    var keys1 = Object.keys(params1).sort(),
        keys2 = Object.keys(params2).sort(),
        added = [],
        deleted = [],
        changed = [],
        equals = [];
    keys1.forEach(function (key1, i, arr) {
        var key2 = keys2.shift();
        if (typeof(key2) == 'undefined') {
            added.push(key1);
        } else {
            if (key2 < key1) {
                deleted.push(key2);
                while (keys2.length) {
                    key2 = keys2.shift();
                    if (key2 >= key1) {
                        break;
                    } else {
                        deleted.push(key2);
                    }
                }
            }
            if (key1 == key2) {
                if (params1[key1].value == params2[key2].value) {
                    equals.push(key1);
                } else {
                    changed.push(key1);
                }
            }

            if (key1 < key2) {
                added.push(key1);
                keys2.unshift(key2);
            }

        }
    })
    deleted = deleted.concat(keys2);
    return new CompareResult(added, deleted, changed, equals);
}
/**
 *
 * @param $form { {jQuery}}
 * @returns {*}
 */
function getFormParams($form) {
    var serializeFormData = $form.serialize();
    return parseQuery('?' + serializeFormData);
}

/**
 *
 * @param $form1 {jQuery}
 * @param $form2 {jQuery}
 * @returns {CompareResult}
 */
function compareForms($form1, $form2) {
    var form1Params = getFormParams($form1),
        form2Params = getFormParams($form2);

    console.log(form1Params, form2Params)
    return compareForm(form1Params, form2Params);
}