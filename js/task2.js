/**
 * Created by Семен on 28.05.14.
 */
function MockHttpRequest(headers, url) {
    this.headers = headers;
    this.url = url;
    this._queryParams = null;
}

MockHttpRequest.prototype.getQueryParams = function () {
    if (this._queryParams == null) {
        this._queryParams = parseQuery(this.url);
    }
    return this._queryParams;
};
/**
 *
 * @param url {String}
 * @param httpRequest {MockHttpRequest}
 */
function addQueryParams(url, mockHttpRequest) {
    var result = url,
        hasQueryParams = (url.split(separators.QUERY).length > 1) ? true : false,
        extraQueryParams = mockHttpRequest.getQueryParams(),
        extraQueryParamsLength =Object.keys(extraQueryParams).length;

    if (!hasQueryParams && extraQueryParamsLength) {
        result += separators.QUERY;
    }else if(extraQueryParamsLength){
        result += separators.VARS;
    }
    var rawExtraParams = [];
    for (var i in extraQueryParams) {
        /**
         *
         * @type {QueryParam}
         */
        var queryParam = extraQueryParams[i];
        rawExtraParams.push(
            encodeURIComponent(queryParam.name) + separators.PAIR + encodeURIComponent(queryParam.value)
        );

    }
    result += rawExtraParams.join(separators.VARS);
    return result
}