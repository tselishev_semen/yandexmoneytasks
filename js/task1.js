/**
 * Created by Семен on 28.05.14.
 */
function QueryParam(name, value) {
    this.name = name;
    this.value = value;
}

var separators = {
    QUERY: '?',
    VARS: '&',
    PAIR: '='
};
/**
 *
 * @param query {String}
 */
function parseQuery(query) {
    var rawQuery = query.split(separators.QUERY),
        result = [];
    if (rawQuery.length == 2 && rawQuery[1]) {
        var vars = rawQuery[1].split(separators.VARS);
        vars.forEach(function (item, i, arr) {
            var pair = item.split(separators.PAIR);
            if (pair.length == 2) {
                var name = decodeURIComponent(pair[0]);
                result[name] = new QueryParam(
                    name,
                    decodeURIComponent(pair[1])
                );
            } else {
                throw 'Incorrect Query String';
            }
        })
    }
    return result;

}